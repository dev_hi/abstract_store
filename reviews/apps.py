from django.apps import AppConfig


class ReviewsConfig(AppConfig): # 리뷰 뷰에 대한 정의
    name = 'reviews'
