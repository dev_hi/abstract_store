import random
from django.core.management.base import BaseCommand
from django.contrib.admin.utils import flatten
from django_seed import Seed
from qnas import models as qna_models
from users import models as user_models
from items import models as item_models

class Command(BaseCommand):
    
    help = "This comman creates many questions"
    
    def add_arguments(self, parser):
        parser.add_argument(
            "--number", default=2, type=int, help = "How many questions do you want to create"
        )
        
    def handle(self, *args, **options):
        number = options.get("number")
        seeder = Seed.seeder()
        user = user_models.User.objects.filter(is_superuser = False)
        item = item_models.Item.objects.all()
        
        seeder.add_entity(qna_models.Question, number,{
            "answer_status": lambda x: random.choice(("미답변", "답변완료")),
            "title": lambda x: seeder.faker.text(10),
            "content": lambda x: seeder.faker.text(30),
            "item": lambda x: random.choice(item),
            "user": lambda x: random.choice(user),
        })
        seeder.execute()            
        self.stdout.write(self.style.SUCCESS(f"{number} questions created!!"))