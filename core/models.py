from django.db import models
from . import managers

class TimeStampedModel(models.Model): # 시간을 붙여서 정의할 모델
    
    """ Time Stamped Model """
    
    created = models.DateTimeField(auto_now_add=True, null = True)
    updated = models.DateTimeField(auto_now=True)
    objects = managers.CustomModelManager()
    
    class Meta:
        abstract = True