from django.contrib.auth.models import AbstractUser
from phone_field import PhoneField
from django.shortcuts import reverse
from django.db import models
from core import models as core_models
from orders.models import Order

# Create your models here.


class User(AbstractUser): # 유저 모델에 대한 정의
    # 성별에 대한 정의
    GENDER_MALE = "male"
    GENDER_FEMALE = "female"

    GENDER_CHOICES = (
        (GENDER_MALE, "Male"),
        (GENDER_FEMALE, "Female"),
    )

    # 필드들에 대한 정의
    name = models.CharField(max_length=10) # 이름
    gender = models.CharField(choices=GENDER_CHOICES, # 성별
                              max_length=10, blank=True)
    address1 = models.CharField(max_length=1024) # 주소
    address2 = models.CharField(max_length=1024) # 상세주소
    postal_code = models.CharField(max_length=7, blank=True) # 우편 번호
    birthdate = models.DateField(null=True) # 생일

    email_confirmed = models.BooleanField(default=False) # 이메일 인증을 받았는지 여부
    email_secret = models.CharField(max_length=20, default="", blank=True) # 이메일

    home_number = PhoneField(blank=True, help_text='Contact home number') # 집 전화 번호
    phone_number = PhoneField(blank=True, help_text='Contact phone number') # 전화 번호

    current_order = models.ForeignKey("orders.Order", related_name="users", on_delete=models.SET_NULL, blank=True, null=True) # 현재 주문
    favlist = models.ManyToManyField("items.Item", related_name="favs" , blank=True) # 관심 목록
    
    def get_absolute_url(self): # 유저 정보에 대한 요청시 처리할 메소드
        return reverse("users:profile")
    
    def get_total_order_price(self): # 전체 주문에 대한 가격 반환할 메소드
        total = 0
        for order in self.orders.all():
            total += order.GetActualPrice()
        return total
    
    def get_total_order_count(self): # 전체 주문의 갯수를 반환할 메소드
        return self.orders.all().count()
    
    def get_pay_confirmed(self): # 모든 주문에 대해 완료 할 메소드
        count = 0
        for order in self.orders.all():
            if order.state == "pay_confirmed":
                count += 1
        return count
    
    def get_on_delivery(self): # 주문에 맞게 배달 중이라고 알릴 메소드
        count = 0
        for order in self.orders.all():
            if order.state == "on_delivery":
                count += 1
        return count

    def get_wait_submit(self): # 주문확인에 대한 메소드
        count = 0
        for order in self.orders.all():
            if order.state == "wait_submit":
                count += 1
        return count
    
    def get_ended_delivery(self): # 배달 완료에 대한 메소드
        count = 0
        for order in self.orders.all():
            if order.state == "ended_delivery":
                count += 1
        return count
    
    def get_pay_canceled(self): # 결제 취소에 대한 메소드
        count = 0
        for order in self.orders.all():
            if order.state == "pay_canceled":
                count += 1
        return count