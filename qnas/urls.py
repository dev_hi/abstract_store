from django.urls import path
from . import views

app_name = "qnas"

urlpatterns = [ # 질의응답과 관련된 url 리스트
    path("F/", views.QnAFView.as_view(), name="list-f"),
    path("T/", views.QnATView.as_view(), name="list-t"),
    path("question/create/<int:item_pk>/", views.CreateQuestion.as_view(), name="create-q"),
    path("question/<int:question_pk>/delete/", views.delete_question, name="delete-q"),
    path("question/<int:question_pk>/edit/", views.EditQuestionView.as_view(), name="edit-q"),
    path("answer/create/<int:question_pk>/", views.CreateAnswer.as_view(), name="create-a"),
    path("answer/<int:answer_pk>/delete/", views.delete_answer, name="delete-a"),
    path("answer/<int:answer_pk>/edit/", views.EditAnswerView.as_view(), name="edit-a"),
]
