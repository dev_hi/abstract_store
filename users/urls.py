from django.urls import path
from . import views

app_name = "users"

urlpatterns = [ # users와 관련된 url 리스트에 대한 정의
    path("login/", views.LoginView.as_view(), name="login"),
    path("logout/", views.log_out, name="logout"),
    path("signup/", views.SignUpView.as_view(), name="signup"),
    path("<int:item_pk>/", views.toggle_fav, name="toggle-favs"),
    path("favs/", views.SeeFavsView.as_view(), name="see-favs"),
    path("profile/", views.UserProfileView.as_view(), name="profile"),
    path("update-profile/", views.UpdateProfileView.as_view(), name="update"),
    path("update-password/", views.UpdatePasswordView.as_view(), name="password"),
    path("admin-profile/", views.AdminProfileView.as_view(), name="admin-profile"),
]
