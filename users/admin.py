from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from . import models

# Register your models here.
@admin.register(models.User) #decorator
class CustomUserAdmin(UserAdmin): # 커스텀 어드민에 대한 정의
    fieldsets = UserAdmin.fieldsets + ( # 어드민이 가질 필드에 대한 정의
        (
            "Detail Info",
            {
                "fields" : (
                    "name", # 이름
                    "address1", # 주소
                    "address2", # 상세주소
                    "postal_code", # 우편번호
                    "home_number", # 집전화
                    "phone_number", # 전화번호
                    "favlist", # 관심 리스트
                    "current_order", # 현재 주문
                )
            }
        ),
    )
    
    list_display = (
        "username", # 이름
        "email", # 이메일
        "is_staff", # 관리자인가
        
    )
    
    filter_horizontal = (
        "favlist", # 관심 리스트
    )
    