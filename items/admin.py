from django.contrib import admin
from django.utils.html import mark_safe
from . import models

# Register your models here.
class PhotoInline(admin.TabularInline): # 상품 이미지
    model = models.Photo
    
@admin.register(models.Item)
class ItemAdmin(admin.ModelAdmin): # 상품 관리와 관련된 관리자 클래스
    inlines= (
        PhotoInline,
    )
    
    fieldsets = (
        (
            "Item Info",
            {
                "fields" : (
                    "name",
                    # "shorten_description",
                    # "tag",
                    "category",
                    # "status",
                    "price",
                    "description",
                    "count",
                    "owner",
                )
            }
        ),
    )
    
    list_display = (
        "name",
        # "status",
        "owner",
    )
    
    list_filter = (
        "name",
        # "status",
        "owner",
        # "tag",
        "category",
        "price",
        # "status",
    )
    
    filter_horizontal = (
        # "tag",
        "category",
    )

@admin.register(models.Tag)
class TagAdmin(admin.ModelAdmin): # 태그와 관련된 관리자 클래스
    fieldsets = (
        (
            "Tag Info",
            {
                "fields" : (
                    "tag_name",
                )
            }
        ),
    )
    
@admin.register(models.Option)
class OptionAdmin(admin.ModelAdmin): # 선택사항과 관련된 관리자 클래스
    fieldsets = (
        (
            "Option Info",
            {
                "fields" : (
                    "name",
                    "extra_money",
                    "item",
                )
            }
        ),
    )
    
    list_display = (
        "name",
        "extra_money",
        "item",
    )

@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin): # 분류와 관련된 관리자 클래스
    fieldsets = (
        (
            "Category Info",
            {
                "fields" : (
                    "category_name",
                )
            }
        ),
    )
    
    list_display = (
        "category_name",
    )

@admin.register(models.Photo)
class PhotoAdmin(admin.ModelAdmin): # 상품 사진과 관련된 관리자 클래스
    fieldsets = (
        (
            "Photo Info",
            {
                "fields" : (
                    "file",
                )
            }
        ),
        (
            "Related Info",
            {
                "fields" : (
                    "item",
                )
            }
        ),
    )
    
    list_display = (
        "get_photo",
    )
    
    def get_photo(self, obj): # 사진을 반환할 메소드
        return mark_safe(f'<img width="50px" src="{obj.file.url}"/>')
    get_photo.short_description = "Photo"