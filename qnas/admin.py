from django.contrib import admin
from . import models

# Register your models here.
# class PhotoInline(admin.TabularInline):
#     model = models.Photo
    
@admin.register(models.Question)
class QuestionAdmin(admin.ModelAdmin): # 문의사항에 관한 어드민 클래스
    # inlines= (
    #     PhotoInline,
    # )
    
    fieldsets = ( # 가질 필드
        (
            "Question Info",
            {
                "fields" : (
                    "answer_status",
                    "title",
                    "content",
                    "user",
                    "item",
                )
            }
        ),
    )
    
    list_display = ( # 나타낼 필드
        "answer_status",
        "title",
        "content",
        "user",
        "item",
    )
    
    list_filter = ( # 필터 리스트
        "answer_status",
        "user",
        "item",
    )
    
@admin.register(models.Answer)
class AnswerAdmin(admin.ModelAdmin): # 답변에 관해 관리자가 관리할 것을 나타낸 클래스
    fieldsets = ( # 관리할 필드 정의
        (
            "Answer Info",
            {
                "fields" : (
                    "title",
                    "content",
                    "user",
                )
            }
        ),
        (
            "Info",
            {
                "fields" : (
                    "question",
                )
            }
        ),
    )
    
    list_display = ( # 보여줄 필드
        "title",
        "content",
        "user",
    )
    
    list_filter = ( # 필드 필터
        "user",
    )
    
# @admin.register(models.Photo)
# class PhotoAdmin(admin.ModelAdmin):
#     fieldsets = (
#         (
#             "Photo Info",
#             {
#                 "fields" : (
#                     "file",
#                 )
#             }
#         ),
#         (
#             "Related Info",
#             {
#                 "fields" : (
#                     "question",
#                 )
#             }
#         ),
#     )
    
#     list_display = (
#         "get_photo",
#     )
    
#     def get_photo(self, obj):
#         return mark_safe(f'<img width="50px" src="{obj.file.url}"/>')
#     get_photo.short_description = "Photo"