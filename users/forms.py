from django import forms
from django.contrib.auth.forms import UserCreationForm
from . import models


class LoginForm(forms.Form): # 로그인 폼

    email = forms.EmailField(widget=forms.EmailInput(
        attrs={"placeholder": "Email"})) # 이메일
    
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={"placeholder": "Password"}) # 비밀 번호
    )
    email.label="이메일"
    password.label="비밀번호"

    def clean(self): # 지워진 데이터
        email = self.cleaned_data.get("email")
        password = self.cleaned_data.get("password")
        try:
            user = models.User.objects.get(email=email)
            if user.check_password(password):
                return self.cleaned_data
            else:
                self.add_error(
                    "password", forms.ValidationError("Password is wrong"))
        except models.User.DoesNotExist:
            self.add_error("email", forms.ValidationError(
                "User does not exist"))


class SignUpForm(forms.ModelForm): # 회원 가입 폼
    class Meta:
        model = models.User # 유저 모델 그대로 사용해서 포함
        fields = ( # 사용할 필드
            "email",
            "name",
            "birthdate",
        )
        widgets = { # 사용할 위젯
            "email": forms.EmailInput(attrs={"placeholder": "이메일 주소를 입력하세요"}),
            "name": forms.TextInput(attrs={"placeholder": "이름을 입력하세요"}),
            "birthdate": forms.DateTimeInput(attrs={"placeholder": "YY-MM-DD"}),
        }
        
        # 입력창
    email = forms.CharField(widget=forms.EmailInput(attrs={"placeholder": "이메일 주소를 입력하세요"})) 
    name = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "이름을 입력하세요"}))
    birthdate = forms.DateField(widget=forms.DateTimeInput(attrs={"placeholder": "YY-MM-DD"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder": "Password"}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder": "Confirm Password"}))
    
    email.label = "이메일"
    name.label = "이름"
    birthdate.label = "생년월일"
    password.label = "비밀번호"
    password1.label = "비밀번호 확인"

    def clean_password1(self): # 비밀번호 검사 후 다르면 지우고 에러 출력하는 메소드
        password = self.cleaned_data.get("password")
        password1 = self.cleaned_data.get("password1")

        if password != password1:
            raise forms.ValidationError("비밀번호를 확인해주세요.")
        else:
            return password

    def save(self, *args, **kwrgs): # 저장 하는 메소드
        user = super().save(commit=False)
        email = self.cleaned_data.get("email")
        password = self.clean_password1()
        name = self.cleaned_data.get("name")
        # name = self.get("name")
        user.name = name
        user.username = email
        user.set_password(password)
        user.save()
