from django import forms
from . import models

class CreateItemForm(forms.ModelForm): # 상품 등록 폼
    class Meta:
        model = models.Item
        fields=(
            "category",
            "name",
            "price",
            "count",
            "description",
        )
        
    def save(self, *args, **kwargs):
        item = super().save(commit=False)
        return item
    
class CreatePhotoForm(forms.ModelForm): # 상품 사진을 등록할 폼
    
    class Meta:
        model = models.Photo
        fields = (
            "file",
        )
        
    def save(self, pk, *args, **kwargs): # 사진 저장과 관련된 메소드
        photo = super().save(commit=False)
        item = models.Item.objects.get(pk=pk)
        photo.item = item
        photo.save()
        
class CreateCategoryForm(forms.ModelForm): # 상품 종류를 지정하는 뷰
    
    class Meta:
        model = models.Category
        fields = (
            "category_name",
        )    
    category_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "카테고리 이름을 입력해주세요"}))
    category_name.label = "카테고리명"
    
    def save(self, *args, **kwargs):
        category = super().save(commit=False)
        category.save()
        
class CreateTagForm(forms.ModelForm): # 상품 태그를 만드는 뷰
    
    class Meta:
        model = models.Tag
        fields = (
            "tag_name",
        )
    tag_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "태그 이름을 입력해주세요"}))
    tag_name.label = "태그명"
        
    def save(self, *args, **kwargs): # 태그 저장 메소드
        tag = super().save(commit=False)
        tag.save()

class CreateOptionForm(forms.ModelForm): # 상품 선택사항을 추가하는 폼
    
    class Meta:
        model = models.Option
        fields = (
            "name",
            "extra_money",
        )
    name = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "옵션 이름을 입력해주세요"}))
    name.label = "옵션명"
    extra_money = forms.IntegerField()
    extra_money.label = "추가금"
        
    def save(self, item_pk, *args, **kwargs): # 상품 선택사항을 저장하는 메소드
        item = models.Item.objects.get(pk=item_pk)
        option = super().save(commit=False)
        option.item = item
        option.save()