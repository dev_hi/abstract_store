from django.apps import AppConfig


class UsersConfig(AppConfig): # 유저 설정에 대한 클래스
    name = 'users'
