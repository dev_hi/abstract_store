from users.models import User
from items.models import Item, Option
from orders.models import Order, OrderingItem, Payment
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer): # 유저 직렬화 클래스
    id = serializers.SerializerMethodField()
    username = serializers.CharField(read_only=True)
    email = serializers.EmailField(read_only=True)
    current_order = serializers.PrimaryKeyRelatedField(queryset=Order.objects.all())
    favlist = serializers.PrimaryKeyRelatedField(queryset=Item.objects.all(), many=True)


    class Meta:
        model = User
        fields = [
            'id', 
            'username', 
            'email', 
            'gender', 
            'address1', 
            'address2', 
            'postal_code', 
            'email_confirmed', 
            'home_number',
            'phone_number', 
            'birthdate', 
            'current_order', 
            'favlist'
        ]

    def get_id(self, obj):
        return obj.pk

class ItemSerializer(serializers.ModelSerializer): # 상품 직렬화 클래스
    class Meta:
        model = Item
        fields = ['name', 'shorten_description', 'status', 'price', 'description', 'count']


class OrderingItemSerializer(serializers.ModelSerializer): # 주문에 포함된 상풍 직렬화 클래스
    item = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = OrderingItem
        fields = ['item', 'option', 'quantity']
    

class OrderSerializer(serializers.ModelSerializer): # 주문 직렬화 클래스
    items = OrderingItemSerializer(many=True)
    total_price = serializers.SerializerMethodField()
    order_id = serializers.SerializerMethodField()
    payment = serializers.PrimaryKeyRelatedField(queryset=Payment.objects.all())
    class Meta:
        model = Order
        fields = ['order_id', 'user', 'items', 'state', 'receipt_id', 'address', 'postal_code', 'confirmed_date', 'delivery_started_date', 'completed_date', 'delivery_id', 'total_price', 'payment']
    
    def get_total_price(self, obj):
        return obj.GetActualPrice()

    def get_order_id(self, obj):
        return obj.pk


class PaymentSerializer(serializers.ModelSerializer): # 결제방식 직렬화 클래스
    order_id = serializers.PrimaryKeyRelatedField(queryset=Order.objects.all())
    class Meta:
        model = Payment