# Generated by Django 2.2.5 on 2020-06-23 22:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('items', '0008_auto_20200428_2101'),
        ('orders', '0004_order_delivery_id'),
        ('users', '0005_user_postal_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='current_order',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='users', to='orders.Order'),
        ),
        migrations.AddField(
            model_name='user',
            name='favlist',
            field=models.ManyToManyField(blank=True, related_name='favs', to='items.Item'),
        ),
        migrations.DeleteModel(
            name='FavList',
        ),
    ]
