from django import template
from items import models as item_models

register = template.Library()

@register.simple_tag()
def get_categories():
    category = item_models.Category.objects.all()
    return category

