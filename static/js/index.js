function GetSessOrLogin(cb) {
    StoreAPI.GetObject().then(function (s) { 
        console.log('success');
        cb(s);
    }).catch(function () {
        window.location.href = "/users/login"; 
    });
}

function btcreate(e){
    if(e != undefined) {
        var target = e.currentTarget;
        var val = target.options[target.selectedIndex].value.split(",");
        var count = val[0];
        var name = val[1];
        var money = parseInt(val[2]) + parseInt(val[3]);
        
        if(document.getElementById(count) == null){
            var div_1 = document.createElement("div");
            div_1.className = "px-2 mb-2 flex justify-between";
            var option_name = document.createElement("span");
            option_name.className = "font-semibold";
            option_name.appendChild(document.createTextNode(name));
            var bt_x = document.createElement("button");           
            bt_x.setAttribute("onclick","btdelete(" + count + ")");
            bt_x.className = "text-center font-semiblod focus:outline-none";
            bt_x.appendChild(document.createTextNode("X"));
            div_1.appendChild(option_name);
            div_1.appendChild(bt_x);
    
            var div_2 = document.createElement("div");
            div_2.className = "flex";
            var bt_minus = document.createElement("button");
            bt_minus.setAttribute("onclick","btclick(" + count + ",0)");
            bt_minus.className="border text-center w-6 bg-white font-semiblod";
            bt_minus.appendChild(document.createTextNode("-"));
            var input_data = document.createElement("input");
            input_data.id = count;
            input_data.type = "text";
            input_data.className = "option-quantity border-t border-b text-center w-6 text-sm font-semibold";
            input_data.value = "1";
            var bt_plus = document.createElement("button");
            bt_plus.setAttribute("onclick","btclick(" + count + ",1)");
            bt_plus.className="border text-center w-6 bg-white font-semiblod";
            bt_plus.appendChild(document.createTextNode("+"));
            div_2.appendChild(bt_minus);
            div_2.appendChild(input_data);
            div_2.appendChild(bt_plus);
    
            var price = document.createElement("span");
            price.id= count + "-price"
            price.value = money;
            money = String(money).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            price.innerHTML = money + "원";
    
            var div_3 = document.createElement("div");
            div_3.className="flex justify-between px-2 items-center";
            div_3.appendChild(div_2);
            div_3.appendChild(price);
    
            var body = document.createElement("div");
            body.setAttribute('data-option', count);
            body.className = "option w-full bg-gray-200 rounded-md mb-2 py-3 px-2";
            body.id = count + "-body";
            
            body.appendChild(div_1);
            body.appendChild(div_3);
    
            var par = document.getElementById("par");
            par.appendChild(body);
        }else{
            btclick(count, "+");
        }
        target.selectedIndex=0;
    }
}

function btclick(id,mode){
    var data = document.getElementById(id);
    var price_doc = document.getElementById(id + "-price");
    var count = document.getElementById("item_remain");
    var price = parseInt(price_doc.value);
    var cnt = count.innerHTML.split("개")[0];
    if(cnt=="재고 소진"){
        cnt=0
    }

    if(mode == 0){
        if(data.value > 1){
            data.value = parseInt(data.value) - 1;
        } 
    }
    else{
        if(parseInt(data.value) < parseInt(cnt)){
            data.value = parseInt(data.value) + 1;
        }
    }
    price *= data.value;
    price = String(price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원";
    price_doc.innerHTML = price;
}

function btdelete(id){
    var parent = document.getElementById("par");
    var e = document.getElementById(id+"-body");
    parent.removeChild(e);
}

function init(e) {
    let make_order_button = document.getElementById('item_make_order');
    if(make_order_button != null) {
        make_order_button.addEventListener('click', function(e) {
            GetSessOrLogin(function(session) {
                let item_id = StoreAPI.GetItemID();
                session.GetItemCount(item_id).then(function (d) {
                    console.log(d);
                    let ois = [];
                    let options = document.getElementsByClassName('option');
                    let total_amount = 0;
                    for(let i = 0 ; i < options.length; ++i) {
                        let elem = options[i];
                        let elem_quantity = elem.getElementsByClassName('option-quantity')[0];
                        let option_num = parseInt(elem.getAttribute('data-option'));
                        let quantity = parseInt(elem_quantity.value);
                        ois.push({
                            item: item_id,
                            option: option_num,
                            quantity: quantity 
                        });
                        total_amount += quantity;
                        console.log(total_amount, quantity);
                    }
                    console.log(total_amount);
                    if(d.remain < total_amount) {
                        alert('재고량보다 많은 양은 주문하실 수 없습니다.');
                    }
                    else if(ois.length > 0) {
                        session.OrderItems(ois).then(function (order) {
                            window.location.href = "/orders/" + order.order_id; 
                        });
                    }
                })
            });
        });
    }
    
    let order_confirm_pay_button = document.getElementById('order_pay_confirm');
    if(order_confirm_pay_button != null) {
        order_confirm_pay_button.addEventListener('click', function(e) {
            GetSessOrLogin(function(session) {
                session.OpenPayment(StoreAPI.GetOrderID());
                //session.OpenIframe('/orders/' + StoreAPI.GetOrderID() + '/confirm');
            });
        });
    }

    let bt_create = document.getElementById('select');
    if(bt_create != null) {
        bt_create.addEventListener('change', btcreate);
    }

    let favlist_add_button = document.getElementById('favlist_add');
    if(favlist_add_button != null) {
        favlist_add_button.addEventListener('click', function() {
            GetSessOrLogin(function(session) {
                session.AddFav(StoreAPI.GetItemID()).then(function() {
                    location.reload(true);
                });
            });
        });
    }

    let favlist_remove_button = document.getElementById('favlist_remove');
    if(favlist_remove_button != null) {
        favlist_remove_button.addEventListener('click', function() {
            GetSessOrLogin(function(session) {
                session.RemoveFav(StoreAPI.GetItemID()).then(function() {
                    location.reload(true);
                });
            });
        });
    }

    let admin_item_count_button = document.getElementById('item_count_add_btn');
    if(admin_item_count_button != null) {
        admin_item_count_button.addEventListener('click', function() {
            GetSessOrLogin(function(session) {
                let admin_item_count_field = document.getElementById('admin_item_count');
                if(admin_item_count_field != null) {
                    session.AddItemCount(admin_item_count_field.getAttribute('data-id'), parseInt(admin_item_count_field.value)).then(function() {
                        location.reload(true);
                    });
                }
            });
        });
    }

    let admin_item_count_buttons = document.getElementsByClassName('item-count-add-btn');
    for(let i = 0; i < admin_item_count_buttons.length; ++i) {
        admin_item_count_buttons[i].addEventListener('click', function(e) {
            var target = e.currentTarget;

            GetSessOrLogin(function(session) {
                var target_input = document.getElementById(target.getAttribute('data-input-id'));
                var target_id = target_input.getAttribute('data-id');
                session.AddItemCount(target_id, parseInt(target_input.value)).then(function() {
                    location.reload(true);
                });
            });
        });
    }


    let order_item_remove_buttons = document.getElementsByClassName('order-item-remove');
    for(let i = 0; i < order_item_remove_buttons.length; ++i) {
        order_item_remove_buttons[i].addEventListener('click', function(e) {
            var target = e.currentTarget;
            GetSessOrLogin(function(session) {
                session.DeleteOrderingItem(target.getAttribute('data-id')).then(function() {
                    location.reload(true);
                });
            });
        });
    }

    let order_set_submitted = document.getElementById('order_set_submitted');
    if(order_set_submitted != null) {
        order_set_submitted.addEventListener('click', function() {
            GetSessOrLogin(function(session) {
                session.SetOrderState(StoreAPI.GetOrderID(), 'wait_submit').then(function() {
                    location.reload(true);
                });
            });
        });
    }

    let order_set_on_delivery = document.getElementById('order_set_on_delivery');
    if(order_set_on_delivery != null) {
        order_set_on_delivery.addEventListener('click', function() {
            GetSessOrLogin(function(session) {
                let delivery_id_input = document.getElementById('delivery_id');
                session.SetOrderDeliveryID(StoreAPI.GetOrderID(), delivery_id_input.value).then(function() {
                    location.reload(true);
                });
            });
        });
    }

    let order_set_completed = document.getElementById('order_set_completed');
    if(order_set_completed != null) {
        order_set_completed.addEventListener('click', function() {
            GetSessOrLogin(function(session) {
                session.SetOrderState(StoreAPI.GetOrderID(), 'ended_delivery').then(function() {
                    location.reload(true);
                });
            });
        });
    }
}

document.addEventListener('DOMContentLoaded', init, false);