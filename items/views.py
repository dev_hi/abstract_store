from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, reverse
from django.views.generic import ListView, DetailView, FormView, UpdateView
from . import models, forms
from users import models as user_models

# Create your views here.
class HomeView(ListView): # 상품 리스트를 보여줄 뷰
    
    model = models.Item
    paginate_by = 12
    paginate_orphans = 5
    ordering = "-created"
    context_object_name = "items"
    
    def get_queryset(self):
        item_category = self.request.GET.get("category")
        search = self.request.GET.get("search")
        if item_category is not None:
            return models.Item.objects.filter(category__category_name=item_category).order_by("-created")
        if search is not None:
            return models.Item.objects.filter(name__icontains=search).order_by("-created")
        return models.Item.objects.all().order_by("-created")
    
class ItemDetail(DetailView): # 상품에 대한 상세 정보를 보여줄 뷰
    
    model = models.Item
    context_object_name = "item"
    
class CreateItemView(FormView): # 상품 생성과 관련된 뷰
    
    form_class = forms.CreateItemForm
    template_name = "items/item_create.html"
    
    def form_valid(self, form):
        item = form.save()
        item.owner = self.request.user
        item.save()
        form.save_m2m()
        return redirect(reverse("items:photos", kwargs={'pk': item.pk}))
    
class EditItemView(UpdateView): # 상품 정보 편집 뷰
    
    model = models.Item
    template_name = "items/item_edit.html"
    fields = (
            "category",
            "name",
            "price",
            "count",
            "description",
    )
    
    def get_success_url(self):
        item_pk = self.kwargs.get("pk")
        if self.request.GET.get("m") == "admin":
            return reverse("items:admin-page")
        return reverse("items:detail", kwargs={"pk":item_pk})
    
@login_required
def delete_item(request, pk): # 상품 삭제 뷰
    try:
        item = models.Item.objects.get(pk=pk)
        user = request.user
        if user == item.owner:
            item.delete()
            pass
        if request.GET.get("m") == "admin":
            return redirect(reverse("items:admin-page"))
        return redirect(reverse("core:home"))
    except models.Item.DoesNotExist:
        pass

class ItemPhotosView(DetailView): # 상품 사진과 관련된 뷰
    
    model = models.Item
    template_name = "items/photo/item_photos.html"
    
    def get_object(self, queryset=None):
        item = super().get_object(queryset=queryset)
        if not self.request.user.is_superuser:
            raise Http404()
        return item

class AddPhotoView(FormView): # 상품 이미지 추가 뷰
    
    template_name = "items/photo/photo_create.html"
    form_class = forms.CreatePhotoForm
    
    def form_valid(self, form):
        pk = self.kwargs.get('pk')
        form.save(pk)
        return redirect(reverse("items:photos", kwargs={'pk':pk}))
    
@login_required
def delete_photo(request, item_pk, photo_pk): # 상품 이미지 삭제 뷰
    user = request.user
    try:
        item = models.Item.objects.get(pk=item_pk)
        if item.owner.pk != user.pk:
            print("Can't delete that photo")
        else:
            models.Photo.objects.filter(pk=photo_pk).delete()
            print("Photo Deleted")
        return redirect(reverse("items:photos", kwargs={'pk':item_pk}))
    except models.Item.DoesNotExist:
        return redirect(reverse("core:home"))
    
class EditPhotoView(UpdateView): # 상품 이미지 편집과 관련된 뷰
    
    model = models.Photo
    template_name = "items/photo/photo_edit.html"
    pk_url_kwarg = "photo_pk"
    success_message = "Photo Updated"
    fields = ("file",)
    
    def get_success_url(self):
        item_pk = self.kwargs.get("item_pk")
        return reverse("items:photos", kwargs={"pk":item_pk})

# class ItemByOuterView(ListView):
#     paginate_by = 12
#     paginate_orphans = 5
#     ordering = "-created"
#     context_object_name = "items"
    
#     def get_queryset(self):
#         item_category = self.kwargs.get("category")
#         return models.Item.objects.filter(category__category_name=item_category)
    
class AdminView(ListView): # 관리자가 보는 상품 뷰
    
    model = models.Item
    template_name = "items/admin/item_admin.html"
    paginate_by = 5
    paginate_orphans = 2
    ordering = "-created"
    context_object_name = "items"
    
    def get_queryset(self):
        search = self.request.GET.get("search")
        if search is None:
            return models.Item.objects.all().order_by("-created")
        return models.Item.objects.filter(name__icontains=search).order_by("-created")
    
# 카테고리
class CategoryView(ListView): # 상품 종류 뷰
    model = models.Category
    template_name = "items/category/category_list.html"
    context_object_name = "categories"
    
    def get_object(self, queryset=None):
        category = super().get_object(queryset=queryset)
        return category

class AddCategoryView(FormView): # 상품 종류를 추가하는 뷰
    
    template_name = "items/category/category_create.html"
    form_class = forms.CreateCategoryForm
    
    def form_valid(self, form):
        form.save()
        return redirect(reverse("items:categories"))
    
@login_required
def delete_category(request, category_pk): # 상품 종류 삭제를 위한 뷰
    user = request.user
    try:
        category = models.Category.objects.get(pk=category_pk)
        if user.is_superuser:
            category.delete()
        return redirect(reverse("items:categories"))
    except models.Item.DoesNotExist:
        return redirect(reverse("core:home"))
    
class EditCategoryView(UpdateView): # 상품 종류 편집 뷰
    
    model = models.Category
    template_name = "items/category/category_edit.html"
    pk_url_kwarg = "category_pk"
    success_message = "Category_pk Updated"
    fields = (
        "category_name",
        )
    
    def get_success_url(self):
        category_pk = self.kwargs.get("category_pk")
        return reverse("items:categories")

# 태그
class TagView(ListView): # 태그를 보여줄 뷰
    model = models.Tag
    template_name = "items/tag/tag_list.html"
    context_object_name = "tags"
    
    def get_object(self, queryset=None):
        tag = super().get_object(queryset=queryset)
        return tag

class AddTagView(FormView): # 태그 추가를 위한 뷰
    
    template_name = "items/tag/tag_create.html"
    form_class = forms.CreateTagForm
    
    def form_valid(self, form):
        form.save()
        return redirect(reverse("items:tags"))
    
@login_required
def delete_tag(request, tag_pk): # 태그 삭제를 위한 메소드
    user = request.user
    try:
        tag = models.Tag.objects.get(pk=tag_pk)
        if user.is_superuser:
            tag.delete()
        return redirect(reverse("items:tags"))
    except models.Item.DoesNotExist:
        return redirect(reverse("core:home"))
    
class EditTagView(UpdateView): # 태그 퍈집을 위한 뷰
    
    model = models.Tag
    template_name = "items/tag/tag_edit.html"
    pk_url_kwarg = "tag_pk"
    success_message = "tag_pk Updated"
    fields = (
        "tag_name",
        )
    
    def get_success_url(self):
        tag_pk = self.kwargs.get("tag_pk")
        return reverse("items:tags")
    
# 옵션
class OptionView(ListView): # 상품 상세조건을 보여줄 뷰
    model = models.Option
    template_name = "items/option/option_list.html"
    context_object_name = "options"
    
    def get_queryset(self):
        item_pk = self.kwargs.get("item_pk")
        option = models.Item.objects.get(pk=item_pk).options.all()
        option.pk = item_pk
        if not self.request.user.is_superuser:
            raise Http404()
        return option

class AddOptionView(FormView): # 상세 조건 추가 뷰
    
    template_name = "items/option/option_create.html"
    form_class = forms.CreateOptionForm
    
    def form_valid(self, form):
        item_pk = self.kwargs.get("item_pk")
        form.save(item_pk)
        return redirect(reverse("items:options", kwargs={"item_pk":item_pk}))
    
@login_required
def delete_option(request, option_pk): # 상세 조건 삭제 뷰
    user = request.user
    try:
        option = models.Option.objects.get(pk=option_pk)
        if user.is_superuser:
            option.delete()
        return redirect(reverse("items:options", kwargs={"item_pk":option.item.pk}))
    except models.Item.DoesNotExist:
        return redirect(reverse("core:home"))
    
class EditOptionView(UpdateView): # 상세조건 편집 뷰
    
    model = models.Option
    template_name = "items/option/option_edit.html"
    pk_url_kwarg = "option_pk"
    success_message = "option_pk Updated"
    fields = (
        "name",
        "extra_money"
        )
    
    def get_object(self): 
        option_pk = self.kwargs.get("option_pk")
        option = models.Option.objects.get(pk=option_pk)
        return option
    
    def get_success_url(self):
        option_pk = self.kwargs.get("option_pk")
        option = models.Option.objects.get(pk=option_pk)
        return reverse("items:options", kwargs={"item_pk":option.item.pk})