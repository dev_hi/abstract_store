import random
from django.core.management.base import BaseCommand
from django.contrib.admin.utils import flatten
from django_seed import Seed
from qnas import models as qna_models
from users import models as user_models

class Command(BaseCommand):
    
    help = "This comman creates many answers"
    
    def add_arguments(self, parser):
        parser.add_argument(
            "--number", default=2, type=int, help = "How many answers do you want to create"
        )
        
    def handle(self, *args, **options):
        # number = options.get("number")
        # seeder = Seed.seeder()
        # question = qna_models.Question.objects.filter(answer_status = "답변완료")
        # user = user_models.User.objects.filter(is_superuser = True)
        
        # seeder.add_entity(qna_models.Answer, number,{
        #     "title": lambda x: seeder.faker.text(10),
        #     "content": lambda x: seeder.faker.text(30),
        #     "question": lambda x: random.choice(question),
        #     "user": lambda x: random.choice(user),
        # })
        # seeder.execute()            
        # self.stdout.write(self.style.SUCCESS(f"{number} answers created!!"))
        
        seeder = Seed.seeder()
        all_users = user_models.User.objects.filter(is_superuser=True)
        all_question = qna_models.Question.objects.filter(answer_status = "답변완료")
        for question in all_question:
            answer = qna_models.Answer.objects.create(
                    title=seeder.faker.text(10),
                    content=seeder.faker.text(30),
                    question=question,
                    user=random.choice(all_users),
                    )
        self.stdout.write(self.style.SUCCESS("answers created!!"))