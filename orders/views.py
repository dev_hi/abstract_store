from django.shortcuts import redirect,reverse
from django.http import HttpResponse
from django.views.generic import TemplateView, ListView, DetailView
from django.views.decorators.csrf import csrf_exempt
from bootpay.BootpayApi import BootpayApi

from items import models as item_models
from . import models
from users.models import User
from users import mixins as user_mixins
from items.models import Item

# Create your views here.

def GetVerified(order, receipt_id): # 주문 수락하는 뷰
    bootpay = BootpayApi()
    tk = bootpay.get_access_token()
    if(tk['status'] is 200):
        # TODO : verifying in this module is temporary. it will be deprecated soon, and verify will proceed in REST.
        result = bootpay.verify(receipt_id)
        if(result['status'] is 200):
            if(order.payment is not None) : order.payment.delete()
            # TODO : verify information must saved in DB
            payment = models.Payment.objects.create(
                receipt_id = result['data']['receipt_id'],
                order_id = order,
                name = result['data']['name'],
                price = result['data']['price'],
                tax_free = result['data']['tax_free'],
                remain_price = result['data']['remain_price'],
                remain_tax_free = result['data']['remain_tax_free'],
                cancelled_price = result['data']['cancelled_price'],
                cancelled_tax_free = result['data']['cancelled_tax_free'],
                receipt_url = result['data']['receipt_url'],
                unit = result['data']['unit'],
                pg = result['data']['pg'],
                method = result['data']['method'],
                pg_name = result['data']['pg_name'],
                method_name = result['data']['method_name'],
                requested_at = result['data']['requested_at'],
                purchased_at = result['data']['purchased_at'],
                payment_data = str(result['data']['payment_data']),
                status = result['data']['status'],
                status_en = result['data']['status_en'],
                status_ko = result['data']['status_ko']
            )
            payment.save()
            order.payment = payment
            order.receipt_id = result['data']['receipt_id']
            if order.state == order.ORDER_CREATED:
                if payment.status is 1:
                    order.state = order.PAY_CONFIRMED
                elif payment.status is 20:
                    order.state = order.PAY_CANCELED
                else:
                    order.state = order.ORDER_CREATED
            order.save()
            return payment
    return None

class OrderDetail(user_mixins.LoggedInOnlyView, DetailView): # 주문 상세 내역을 확인하기 위한 뷰
    model = models.Order
    context_object_name = 'current_order'
    template_name = "orders/order_detail.html"

    def get_context_data(self, **kwargs):
        context = super(OrderDetail, self).get_context_data(**kwargs)
        context['kwargs'] = kwargs
        if kwargs['object'].payment is not None :
            context['receipt_id'] = kwargs['object'].payment.receipt_id
            vpay = GetVerified(kwargs['object'], kwargs['object'].payment.receipt_id)
            context['payment'] = vpay
            context['receipt_url_replaced'] = vpay.receipt_url.replace('/DefaultWebApp', '')
        #context['verified_payment'] = Task.objects.filter(list=self.object)
        return context

class OrderList(user_mixins.LoggedInOnlyView, ListView): # 주문내역을 보여줄 뷰
    model = models.Order
    context_object_name = 'order_list'
    template_name = "orders/order_list.html"
    def get_queryset(self):
        if self.request.user.is_superuser : return models.Order.objects.all()
        return super(OrderList, self).get_queryset().filter(user=self.request.user)

@csrf_exempt
def order_callback(request, order_id): # 콜백함수
    order = None
    try:
        order = models.Order.objects.get(pk=order_id)
    except:
        return HttpResponse('orderid=' + str(order_id))

    receipt_id = request.POST.get('receipt_id', '')
    code = request.POST.get('code', '')
    status = request.POST.get('status', '')
    if(status == '1'):
        result = GetVerified(order, receipt_id)
        if result is not None:    
            if(result.status is 1):
                if(result.price == order.GetPriceWithDeliveryFee()):
                    user = User.objects.get(current_order=order)
                    user.current_order = None
                    user.save()
                    return redirect(reverse('orders:complete'))
    else:
        if(code == '-100'):
            return redirect(reverse('orders:cancel'))
        if(code == '-102'):
            return redirect(reverse('orders:cancel'))

    return redirect(reverse('orders:error'))

class order_cancel_notify(TemplateView):  # 주문취소를 알릴 뷰
    template_name = "orders/order_payment_cancel.html"
    
class order_pay_complete_notify(TemplateView): # 결재 완료를 알릴 뷰
    template_name = "orders/order_payment_complete.html"

class order_error_notify(TemplateView): # 결제 오류를 알릴 뷰
    template_name = "orders/error.html"
    
def delete_order(request, order_pk): # 주문 삭제를 위한 메소드
    try:
        order = models.Order.objects.get(pk=order_pk)
        user = request.user
        if user == order.user or user.is_superuser:
            for oi in order.items.all():
                oi.delete()
            if order.isPurchased():
                order.cancel_pay()
            order.delete()
            pass
        return redirect(reverse("orders:all"))
    except Item.DoesNotExist:
        pass
    
def cancel_pay(request, order_pk): # 결제 취소를 위한 메소드
    try:
        order = models.Order.objects.get(pk=order_pk)
        user = request.user
        if user == order.user or user.is_superuser:
            order.cancel_pay()
        return redirect(reverse("orders:detail", kwargs={'pk': order_pk}))
    except Item.DoesNotExist:
        pass