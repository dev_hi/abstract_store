from users.models import User
from rest_framework import status
from rest_framework import viewsets, mixins
from items.models import Item, Option
from orders.models import Order, OrderingItem, Payment
from api.serializers import UserSerializer, OrderSerializer, OrderingItemSerializer, PaymentSerializer
from bootpay.BootpayApi import BootpayApi
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.decorators import action
from rest_framework.views import APIView
from django.middleware import csrf
import logging

from users.models import User
from orders.models import Order

logger = logging.getLogger(__name__)

# Create your views here.

def get_access_token_or_503(pk): # 접근 권한 관리 메소드
    order = None
    try:
        order = Order.objects.get(pk=pk)
    except:
        return {'error': Response({'error':'not found'}, 404)}
    if order is None:
        return {'error': Response({'error':'not found'}, 404)}

    bp = BootpayApi()
    result = bp.get_access_token()

    if result['status'] is 200 :
        return {'bootpay':bp, 'result': result, 'order': order}
    else:
        return {'error': Response({'error':'cannot get access token'}, 503)}

class ItemAmountAdderAPIView(APIView): # 아이템 수량 추가 뷰
    def get(self, request, item_id):
        it = Item.objects.get(pk=item_id)
        return Response({'count':it.count, 'remain': it.get_remaining()})

    def put(self, request, item_id):
        it = Item.objects.get(pk=item_id)
        it.count = request.data['count']
        it.save()
        return Response({'count':it.count})

class UserViewSet(viewsets.ModelViewSet): # 유저 뷰
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_session(self, request):
        if request.user.is_anonymous :
            return Response({'error':'unauthorized'}, 401)
        return Response(UserSerializer(request.user).data)

    def put_data(self, request):
        if request.user.is_anonymous :
            return Response({'error':'unauthorized'}, 401)

        user = request.user

        if('favlist' in request.data):
            no_clear = False
            if('items_push' in request.data):
                no_clear = request.data['items_push']

            if no_clear == False:
                for item in user.favlist.all():
                    user.favlist.remove(item)
            
            for item in request.data['favlist']:
                it = Item.objects.get(pk=item)
                user.favlist.add(it)
                
        if('gender' in request.data):
            user.gender = request.data['gender']
        if('address1' in request.data):
            user.address1 = request.data['address1']
        if('address2' in request.data):
            user.address2 = request.data['address2']
        if('postal_code' in request.data):
            user.postal_code = request.data['postal_code']
        if('phone_number' in request.data):
            user.phone_number = request.data['phone_number']
        
        user.save()
        return Response(UserSerializer(user).data)

class PaymentViewSet(viewsets.ModelViewSet): # 결제 리스트 뷰
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer

class OrderingItemView(viewsets.ModelViewSet): # 주문할 상품에 대한 뷰
    queryset = OrderingItem.objects.all()
    serializer_class = OrderingItemSerializer

    def get(self, request, pk):
        if request.user.is_anonymous :
            return Response({'error':'unauthorized'}, 401)
        try:
            oi = OrderingItem.objects.get(pk=pk)
            return Response(OrderingItemSerializer(oi).data)
        except:
            return Response({'error' : 'not found'}, 404)

    def put(self, request, pk):
        if request.user.is_anonymous :
            return Response({'error':'unauthorized'}, 401)
        try:
            oi = OrderingItem.objects.get(pk=pk)
            if 'quantity' in request.data:
                oi.quantity = request.data['quantity']
                oi.save()
            return Response(OrderingItemSerializer(oi).data)
        except:
            return Response({'error' : 'not found'}, 404)

    def delete(self, request, pk):
        oi = None
        try : oi = OrderingItem.objects.get(pk=pk)
        except : return {'error': Response({'error':'not found'}, 404)}
        oi.delete()
        return Response({'delete': 'ok'}, status=200)
    
class OrderAPIView(viewsets.ModelViewSet): # 주문 API 뷰 정의
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def create(self, request): # 생성 메소드
        user = None
        try : user = User.objects.get(pk=request.data['user_id'])
        except : return {'error': Response({'error':'not found'}, 404)}
        order = Order(user=user, address=(user.address1 + ' ' + user.address2), postal_code=user.postal_code)
        order.save()
        
        for item in request.data['items']:
            it = Item.objects.get(pk=item['item'])
            oi = OrderingItem(item=it, quantity=item['quantity'])
            try:
                op = Option.objects.get(item=it)
                oi.option.add(op)
            except:
                pass
            oi.save()
            order.items.add(oi)

        order.save()

        user.current_order = order
        return Response(OrderSerializer(order).data, 200)

    def get_orders(self, request, user_id): # 주문 가져오기
        orders = Order.objects.filter(user=user_id)
        ret = []
        for order in orders:
            ret.append(OrderSerializer(order).data)
        return Response(ret, 200)
    
    def get_current_order(self, request, user_id): # 현재 주문 가져오기
        user = None
        try : user = User.objects.get(pk=user_id)
        except : return Response({'error':'not found'}, 404)
        if user.current_order is None: return Response({'order_id': None}, 200)
        return Response({'order_id': user.current_order.pk}, 200)

    def set_current_order(self, request, user_id): # 현재 주문 설정 메소드
        user = None
        try : user = User.objects.get(pk=user_id)
        except : return Response({'error':'not found'}, 404)
        if user is None: return Response({'error':'not found'}, 404)
        try :
            user.current_order = Order.objects.get(pk=request.data['order_id'])
            user.save()
            return Response({'order_id': user.current_order.pk}, 200)
        except:
            user.current_order = None
            user.save()
            return Response({'order_id': None}, 200)
    
    def retrieve(self, request, pk=None): # GET
        try:
            order = Order.objects.get(pk=pk)
            return Response(OrderSerializer(order).data)
        except:
            return Response({'error' : 'not found'}, 404)

    def update_info(self, request, pk=None): # 정보 수정 메소드
        order = None
        try : order = Order.objects.get(pk=pk)
        except : return {'error': Response({'error':'not found'}, 404)}

        if('items_remove' in request.data):
            idx = request.data['items_remove']
            i = 0
            for item in order.items.all():
                if i == idx : 
                    order.items.remove(item)
                    item.delete()
                    break
                i += 1

        if('items' in request.data):
            no_clear = False
            if('items_push' in request.data):
                no_clear = request.data['items_push']

            if no_clear == False:
                for item in order.items.all():
                    order.items.remove(item)
                    item.delete()
            
            for item in request.data['items']:
                it = Item.objects.get(pk=item['item'])
                oi = OrderingItem(item=it, quantity=item['quantity'])
                oi.save()
                if len(item['option']) > 0:
                    if(item['option'][0] > -1):
                        op = Option.objects.get(pk=item['option'][0])
                        if(op.item == it):
                            oi.option.add(op)
                oi.save()
                order.items.add(oi)
                
        if('state' in request.data):
            if request.data['state'] == order.ENDED_DELIVERY:
                order.SetDeliveryCompleted()
            else:
                order.state = request.data['state']
        if('receipt_id' in request.data):
            order.receipt_id = request.data['receipt_id']
        if('address' in request.data):
            order.address = request.data['address']
        if('postal_code' in request.data):
            order.postal_code = request.data['postal_code']
        if('delivery_id' in request.data):
            order.SetDeliveryID(request.data['delivery_id'])
        if('confirmed_date' in request.data):
            if len(request.data['confirmed_date']) > 0:
                order.confirmed_date = request.data['confirmed_date']
        if('delivery_started_date' in request.data):
            if len(request.data['delivery_started_date']) > 0:
                order.delivery_started_date = request.data['delivery_started_date']
        if('completed_date' in request.data):
            if len(request.data['completed_date']) > 0:
                order.completed_date = request.data['completed_date']
        
        order.save()
        return Response(OrderSerializer(order).data)
        #return Response({'error': 'not valid data'}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None): # 주문 삭제 메소드
        order = None
        try : order = Order.objects.get(pk=pk)
        except : return {'error': Response({'error':'not found'}, 404)}
        order.delete()
        return Response({'delete': 'ok'}, status=200)

    def get_payment(self, request, pk=None): # 주문 결제방식을 가져오는 메소드
        t = self.get_access_token_or_503(pk)
        if 'error' in t:
            return t['error']
        order = t['order']

        resp = t['bootpay'].request_payment({
            'order_id': order.pk,
            'tax_free': 0,
            'price': order.GetPriceWithDeliveryFee(),
            'name': str(order),
            'return_url': 'https://' + request.META['HTTP_HOST'] + '/orders/' + str(order.id) + '/callback/',
            'user_info': {
                'username': order.user.name,
                'email': order.user.email,
                'addr': order.user.address1 + ' ' + order.user.address2,
                'phone': str(order.user.phone_number)
            },
            'params': {
                'csrftoken': csrf.get_token(request)
            }
        })
        if resp['status'] is 200 : 
            return Response({'url':resp['data']})
        else:
            return Response({'error': 'status was not 200', 'data': resp}, 500)

    def verify_receipt(self, request, pk=None): # 영수증 관련 메소드
        t = self.get_access_token_or_503(pk)
        if 'error' in t:
            return t['error']
        order = t['order']
        if order.receipt_id is not None:
            if len(order.receipt_id) > 0:
                data = t['bootpay'].verify(order.receipt_id)
                if(data['status'] is 200):
                    payment = PaymentSerializer(data=data['data'])
                    if(payment.is_valid()):
                        payment.save()
                        return Response({'status':200,'data':data['data']})
                    return Response({'status':500,'data':data['data']}, 500)

        return Response({'verify_status':0})

    def cancel_receipt(self, request, pk=None): # 주문 취소 메소드
        t = self.get_access_token_or_503(pk)
        if 'error' in t:
            return t['error']

        if t['order'].receipt_id is not None:
            if len(t['order'].receipt_id) > 0:
                data = t['bootpay'].cancel(t['order'].receipt_id)
                if(data['status'] is 200):
                    return Response({'cancel':'ok'})

        return Response({'cancel':'fail'})

    def get_access_token_or_503(self, pk): # 권한 확인 메소드 
        order = None
        try:
            order = Order.objects.get(pk=pk)
        except:
            return {'error': Response({'error':'not found'}, 404)}
        if order is None:
            return {'error': Response({'error':'not found'}, 404)}

        bp = BootpayApi()
        result = bp.get_access_token()

        if result['status'] is 200 :
            return {'bootpay':bp, 'result': result, 'order': order}
        else:
            return {'error': Response({'error':'cannot get access token'}, 503)}