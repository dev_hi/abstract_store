import random
from django.core.management.base import BaseCommand
from django_seed import Seed
from users.models import User

class Command(BaseCommand):
    
    help = "This comman creates many users"
    
    def add_arguments(self, parser):
        parser.add_argument(
            "--number", default=2, type=int, help = "How many users do you want to create"
        )
        
    def handle(self, *args, **options):
        number = options.get("number")
        seeder = Seed.seeder()
        seeder.add_entity(User, number, {
            "username" : lambda x: seeder.faker.email(),
            "name" : lambda x: seeder.faker.name(),
            "postal_code" : lambda x: random.randint(10000,100000),
            "is_staff" : False,
            "is_superuser" : False,
            "current_order" : None,
        })
        seeder.execute()
        self.stdout.write(self.style.SUCCESS(f"{number} users created!!"))
            