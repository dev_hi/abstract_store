from django import template
from users.models import User

register = template.Library()

@register.simple_tag(takes_context=True)
def on_favs(context, item):
    user = context.request.user
    user = User.objects.get(pk=user.pk)
    the_list = user.favlist
    if the_list is not None:
        return item in the_list.all()
    return False