from django.shortcuts import render, redirect, reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.views import PasswordChangeView
from django.views.generic import FormView, TemplateView, UpdateView
from django.urls import reverse_lazy
from . import forms, models, mixins
from items import models as item_models
            
class LoginView(mixins.LoggedOutOnlyView, FormView): # 로그인 뷰

    template_name = "users/login.html"
    form_class = forms.LoginForm

    def form_valid(self, form): # 로그인 정보가 확실한지 확인 할 메소드
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password")
        user = authenticate(self.request, username=email, password=password)
        if user is not None:
            login(self.request, user)
        return super().form_valid(form)

    def get_success_url(self): # 성공적으로 로그인 했을때 메소드
        next_arg = self.request.GET.get("next")
        if next_arg is not None:
            return next_arg
        else:
            return reverse("core:home")


def log_out(request): # 로그아웃 메소드
    logout(request)
    return redirect(reverse("core:home"))


class SignUpView(mixins.LoggedOutOnlyView, FormView): # 회원가입 창을 나타낸 메소드
    template_name = "users/signup.html"
    form_class = forms.SignUpForm
    success_url = reverse_lazy("users:update")

    def form_valid(self, form):
        form.save()
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password")
        user = authenticate(self.request, username=email, password=password)

        if user is not None:
            login(self.request, user)

        return super().form_valid(form)

def toggle_fav(request, item_pk): # 관심 목록에서 빼거나 추가하는 메소드
    action = request.GET.get("action", None)
    item = item_models.Item.objects.get_or_none(pk=item_pk)
    user = request.user
    if item is not None and action is not None:
        if action == "add":
            user.favlist.add(item)
        else:
            user.favlist.remove(item)
    return redirect(reverse("items:detail", kwargs={"pk":item_pk}))
    
class SeeFavsView(mixins.LoggedInOnlyView, TemplateView): # 관심 목록을 보여주는 메소드
    template_name = "users/fav/favlist_list.html"
    
class UserProfileView(mixins.LoggedInOnlyView, TemplateView): # 유저 정보를 보여주는 메소드
    template_name = "users/user_detail.html"
    
class UpdateProfileView(mixins.LoggedInOnlyView, UpdateView): # 유저 정보를 갱신하는 메소드
    
    model = models.User
    template_name = "users/update-profile.html"
    fields = (
        "name",
        "gender",
        "address1",
        "address2",
        "postal_code",
        "birthdate",
        "home_number",
        "phone_number",
    )
    
    def get_object(self, queryset=None): # 유저 자체 반환 메소드
        return self.request.user
    
    def get_form(self, form_class=None): # 유저 정보를 보여주는 메소드
        form = super().get_form(form_class=form_class)
        form.fields['name'].widget.attrs= {"placeholder": "Name"}
        form.fields['gender'].widget.attrs= {"placeholder": "Gender"}
        form.fields['address1'].widget.attrs= {"placeholder": "Address1"}
        form.fields['address2'].widget.attrs= {"placeholder": "Address2"}
        form.fields['postal_code'].widget.attrs= {"placeholder": "Postal Code"}
        form.fields['birthdate'].widget.attrs= {"placeholder": "Birthdate"}
        form.fields['home_number'].widget.attrs= {"placeholder": "Home Number"}
        form.fields['phone_number'].widget.attrs= {"placeholder": "Phone Number"}
        form.fields["name"].label = "이름"
        form.fields["gender"].label = "성별"
        form.fields["address1"].label = "주소1"
        form.fields["address2"].label = "주소2"
        form.fields["postal_code"].label = "우편번호"
        form.fields["birthdate"].label = "생년월일"
        form.fields["home_number"].label = "집전화"
        form.fields["phone_number"].label = "휴대전화"
        return form
    
class UpdatePasswordView(mixins.LoggedInOnlyView, PasswordChangeView): # 비밀번호 변경 메소드
    
    template_name = "users/update-password.html"
    
    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)
        form.fields['old_password'].widget.attrs= {"placeholder": "Current password"}
        form.fields['new_password1'].widget.attrs= {"placeholder": "New password"}
        form.fields['new_password2'].widget.attrs= {"placeholder": "Confirm new password"}
        form.fields['old_password'].label = "현재 암호"
        form.fields['new_password1'].label = "새로운 암호"
        form.fields['new_password2'].label = "새로운 암호 확인"
        return form
    
    def get_success_url(self):
        return self.request.user.get_absolute_url()
    
class AdminProfileView(mixins.LoggedInOnlyView, TemplateView): # 관리자 정보를 보여주는 폼을 정의한 메소드
    
    template_name = "users/admin/admin-profile.html"