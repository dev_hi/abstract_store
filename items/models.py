from django.db import models
from core import models as core_models
from ckeditor_uploader.fields import RichTextUploadingField

from orders.models import Order, OrderingItem

# Create your models here.
class Tag(models.Model): # 상품 태그 모델
    tag_name = models.CharField(max_length=1024)
    
    def __str__(self):
        return self.tag_name
    
class Category(models.Model): # 상품 카테고리 모델
    category_name = models.CharField(max_length=1024)
    
    def __str__(self):
        return self.category_name
    
class Option(models.Model): # 상품 선택사항 모델
    name = models.CharField(max_length=1024)
    extra_money = models.IntegerField()
    item = models.ForeignKey("Item", related_name="options", on_delete=models.CASCADE)
    
    def __str__(self):
        return self.name
    
class Item(core_models.TimeStampedModel): # 상품 모델
    name = models.CharField(max_length=80)
    # shorten_description = models.TextField(blank=True)
    # tag = models.ManyToManyField("Tag", related_name="items", blank=True)
    category = models.ManyToManyField("Category", related_name="items", blank=True)
    # status = models.BooleanField(default=True)
    price = models.IntegerField()
    description = RichTextUploadingField(null=True, blank=True)
    count = models.IntegerField()
    owner = models.ForeignKey("users.User", related_name="items", on_delete=models.CASCADE)
    
    # status.boolean = True
    
    def __str__(self):
        return self.name
    
    def thumbnail(self):
        # 미리 보기 이미지 가져오기
        try:
            photo, = self.photos.all()[:1]
            return photo.file.url
        except ValueError:
            return None
        
    def get_photos(self): 
        # 이미지 가져오기 메소드
        try:
            photos = self.photos.all()[0:5]
            return photos
        except ValueError:
            return None
        
    def get_option(self):
        # 선택사항 가져오기 메소드
        try:
            option, = self.options.all()
            return option
        except ValueError:
            return None

    def get_option(self, option_num):
        # n번째 선택사항 가져오기 메소드
        try:
            return self.options.all()[option_num]
        except ValueError:
            return None

    def get_released(self):
        # 판매수량 확인을 위한 메소드
        ret = 0
        ois = OrderingItem.objects.filter(item=self)
        for oi in ois:
            ret += oi.quantity
        return ret

    def get_remaining(self):
        # 남은 삼품수를 확인할 메소드 
        return self.count - self.get_released()

class Photo(models.Model): # 사진에 대한 모델 정의
    file = models.ImageField(upload_to="item_photos")
    item = models.ForeignKey("Item",related_name="photos" , on_delete=models.CASCADE, null = True, blank=True)