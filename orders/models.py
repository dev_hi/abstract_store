from django.db import models
from django.shortcuts import reverse
from core import models as core_models
from django.conf import settings
from bootpay.BootpayApi import BootpayApi
from django.utils import timezone

class Order(core_models.TimeStampedModel): # 주문에 대한 모델 정의
    ORDER_CREATED = "order_created"
    WAIT_SUBMIT = "wait_submit"
    PAY_CONFIRMED = "pay_confirmed"
    PAY_CANCELED = "pay_canceled"
    ON_DELIVERY = "on_delivery"
    ENDED_DELIVERY = "ended_delivery"
    
    STATE_CHOICES = (
        (ORDER_CREATED, "order_created"),
        (WAIT_SUBMIT, "wait_submit"),
        (PAY_CONFIRMED, "pay_confirmed"),
        (PAY_CANCELED, "pay_canceled"),
        (ON_DELIVERY, "on_delivery"),
        (ENDED_DELIVERY, "ended_delivery"),
    )
    
    user = models.ForeignKey("users.User", related_name="orders", on_delete=models.CASCADE)
    items = models.ManyToManyField("orders.OrderingItem", related_name="orders", blank=True)
    state = models.CharField(max_length=50, choices=STATE_CHOICES, default=ORDER_CREATED)
    receipt_id = models.CharField(max_length=24, blank=True)
    address = models.CharField(max_length=128, blank=True, null=True)
    postal_code = models.CharField(max_length=7, blank=True, null=True)
    confirmed_date = models.DateTimeField(blank=True, null=True)
    delivery_started_date = models.DateTimeField(blank=True, null=True)
    completed_date = models.DateTimeField(blank=True, null=True)
    delivery_id = models.CharField(max_length=128, blank=True, null=True)
    payment = models.ForeignKey("orders.Payment", related_name="order_payment", on_delete=models.SET_NULL, blank=True, null=True)
    
    def __str__(self):
        try:
            item_list = self.items.all()
            title = item_list[0].item.name
            if item_list.count() > 0:
                title += " 외 " + str(item_list.count() - 1) + "개의 상품" 
            return title
        except:
            return "상품이 없습니다."
    
    def thumbnail(self): # 주문 이미지에 대한 메소드
        try:
            item = self.items.all()[0]
            photo = item.item.photos.all()[0]
            return photo.file.url
        except:
            return ''

    def GetActualPrice(self): # 주문에 포함된 상품 가격을 보여줄 메소드
        item_list = self.items.all()
        ret = 0
        for it in item_list :
            ret += it.get_price()
        return ret

    def GetDeliveryFee(self): # 배송비를 나타낼 메소드
        return settings.DELIVERY_FEE

    def GetPriceWithDeliveryFee(self): # 배송비와 주문가격을 합쳐서 보여주는 메소드
        return self.GetActualPrice() + self.GetDeliveryFee()
    
    def get_absolute_url(self): # 홈으로 이동시키는 메소드
        return reverse("core:home")

    def isPurchased(self): # 결제가 완료되었는지 확인하는 메소드
        if self.payment is None:
            return False
        else:
            return self.payment.isPayed()

    def cancel_pay(self): # 결제 취소와 관련된 메소드
        if self.payment is not None:
            b = BootpayApi()
            rtk = b.get_access_token()
            if rtk['status'] is 200:
                return b.cancel(self.payment.receipt_id, reason='사용자에 의한 취소')
        return None

    def SetDeliveryID(self, did): # 배송 아이디를 설정하고 시작시간을 나타내는 메소드
        self.delivery_id = did
        self.state = self.ON_DELIVERY
        self.delivery_started_date = timezone.localtime()
    
    def SetDeliveryCompleted(self):
        self.state = self.ENDED_DELIVERY
        self.completed_date = timezone.localtime()
    
class OrderingItem(models.Model): # 주문된 상품을 나타내는 모델
    item = models.ForeignKey("items.Item", related_name="orderingitems", on_delete=models.CASCADE)
    option = models.ManyToManyField("items.Option",related_name="orderingitems", blank=True)
    quantity = models.IntegerField(default=1)
    
    def __str__(self):
        return self.item.name

    def get_price(self): # 상품 가격을 나타내주는 메소드
        # TODO : 옵션... 아이템에... 다수 지정안됨
        ret = 0
        item_option = self.option.all()
        if item_option.count() == 0 : ret = self.item.price
        else: ret = self.item.price + item_option[0].extra_money
        return ret * self.quantity

class Payment(models.Model):
    receipt_id = models.CharField(max_length=24)
    order_id = models.ForeignKey("orders.Order", related_name="payment_order", on_delete=models.CASCADE)
    name = models.TextField(blank=True)
    price = models.IntegerField()
    tax_free = models.IntegerField(default=0)
    remain_price = models.IntegerField(default=0)
    remain_tax_free = models.IntegerField(default=0)
    cancelled_price = models.IntegerField(default=0)
    cancelled_tax_free = models.IntegerField(default=0)
    receipt_url = models.TextField(blank=True)
    unit = models.CharField(max_length=5)
    pg = models.CharField(max_length=20)
    method = models.CharField(max_length=40)
    pg_name = models.TextField(blank=True)
    method_name = models.TextField(blank=True)
    payment_data = models.TextField(blank=True)
    requested_at = models.DateTimeField(blank=True, null=True)
    purchased_at = models.DateTimeField(blank=True, null=True)
    """
    0 - 결제 대기 상태입니다. 승인이 나기 전의 상태입니다.
    1 - 결제 완료된 상태입니다.
    2 - 결제승인 전 상태입니다. transactionConfirm() 함수를 호출하셔서 결제를 승인해야합니다.
    3 - 결제승인 중 상태입니다. PG사에서 transaction 처리중입니다.
    20 - 결제가 취소된 상태입니다.
    -20 - 결제취소가 실패한 상태입니다.
    -30 - 결제취소가 진행중인 상태입니다.
    -1 - 오류로 인해 결제가 실패한 상태입니다.
    -2 - 결제승인이 실패하였습니다.
    
    ORDER_CREATED = "order_created"
    WAIT_CONFIRM = "wait_confirm"
    PAY_CONFIRMED = "pay_confirmed"
    PAY_CANCELED = "pay_canceled"
    ON_DELIVERY = "on_delivery"
    ENDED_DELIVERY = "ended_delivery"
    """
    status = models.IntegerField()
    status_en = models.TextField(blank=True)
    status_ko = models.TextField(blank=True)

    def isPayed(self):
        return self.status == 1
