from django.urls import path
from . import views
from django.conf.urls import url, include
from users.models import User
from rest_framework import routers, serializers, viewsets

app_name = "api" 
 # 주문과 관련된 url 리스트
order_create = views.OrderAPIView.as_view({
    'post': 'create'
})

order_detail = views.OrderAPIView.as_view({
    'get': 'retrieve',
    'put': 'update_info',
    'delete': 'destroy'
})

order_payment = views.OrderAPIView.as_view({
    'get': 'get_payment'
})

orders = views.OrderAPIView.as_view({
    'get': 'get_orders'
})

current_orders = views.OrderAPIView.as_view({
    'get': 'get_current_order',
    'put': 'set_current_order'
})

user = views.UserViewSet.as_view({
    'get': 'get_session',
    'put': 'put_data'
})

payment = views.OrderAPIView.as_view({
    'get': 'verify_receipt',
    'delete': 'cancel_receipt'
})

urlpatterns = [
    url(r'^order/create$', order_create, name='api-order-create'),
    url(r'^user/session$', user, name='api-user-session'),
    url(r'^item/(?P<item_id>[0-9]+)/count', views.ItemAmountAdderAPIView.as_view(), name='api-item-count'),
    url(r'^user/(?P<user_id>[0-9]+)/current_order$', current_orders, name='api-get-user-current-orders'),
    url(r'^user/(?P<user_id>[0-9]+)/orders$', orders, name='api-get-user-orders'),
    url(r'^order/(?P<pk>[0-9]+)/payment/url$', order_payment, name='api-order-payment-url'),
    url(r'^order/(?P<pk>[0-9]+)/payment$', payment, name='api-order-payment'),
    url(r'^order/(?P<pk>[0-9]+)', order_detail, name='api-order-detail'),
    url(r'^oi/(?P<pk>[0-9]+)', views.OrderingItemView.as_view({'get': 'get', 'delete': 'delete', 'put':'put'}), name='api-oi')
]