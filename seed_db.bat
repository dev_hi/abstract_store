@echo off
python manage.py migrate
python manage.py createsuperuser
python manage.py seed_users --number 15
python manage.py seed_categories
python manage.py seed_items --number 30
python manage.py seed_favs
python manage.py seed_reviews --number 50
python manage.py seed_questions --number 50
python manage.py seed_answers