from django.contrib import admin
from . import models

# Register your models here.
@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            "Order Info",
            {
                "fields" : (
                    "user",
                    "items",
                    "state",
                    "receipt_id",
                    "address",
                    "postal_code",
                    "confirmed_date",
                    "delivery_started_date",
                    "completed_date",
                )
            }
        ),
    )
    
    list_display = (
        "__str__",
        "state",
        "user",
        "confirmed_date",
        "delivery_started_date",
        "completed_date",
    )
    
    list_filter = (
        "state",
        "user",
    )

@admin.register(models.OrderingItem)
class OrderingItemAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            "OrderingItem Info",
            {
                "fields" : (
                    "item",
                    "option",
                    "quantity",
                )
            }
        ),
    )
    
    list_display = (
        "item",
        "quantity",
    )
    
    list_filter = (
        "item",
    )
    
@admin.register(models.Payment)
class PaymentAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            "Payment Info",
            {
                "fields" : (
                    "receipt_id",
                    "order_id",
                    "name",
                    "price",
                    "tax_free",
                    "remain_price",
                    "remain_tax_free",
                    "cancelled_price",
                    "cancelled_tax_free",
                    "receipt_url",
                    "unit",
                    "pg",
                    "method",
                    "pg_name",
                    "method_name",
                    "payment_data",
                    "requested_at",
                    "purchased_at",
                    "status",
                    "status_en",
                    "status_ko",
                )
            }
        ),
    )
    
    list_display = (
        "order_id",
        "status",
    )
    
    list_filter = (
        "status",
    )