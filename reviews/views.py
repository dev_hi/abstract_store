from django.http import Http404 
from django.db.models import Q
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.views.generic import FormView, UpdateView, ListView
from items import models as item_models
from . import forms, models

# Create your views here.
def create_review(request, item_pk): # 리뷰생성에 관한 메소드
    title = request.POST['title']
    content = request.POST['content']
    if title != "" and content != "":
        item = item_models.Item.objects.get(pk=item_pk)
        user = request.user
        
        review = models.Review.objects.create(
            item = item,
            user = user,
            title = title,
            content = content,
        )
        review.save()
        return redirect(reverse("items:detail", kwargs={"pk":item.pk}))
    else:
        return redirect(reverse("core:home"))
    
class CreateReview(FormView):  # 리뷰 생성과 관련된 클래스
    form_class = forms.CreateReviewForm
    template_name = "reviews/review_form.html"
    
    def form_valid(self, form):
        pk = self.kwargs.get('item_pk')
        review = form.save(pk)
        review.user = self.request.user
        review.save()
        return redirect(reverse("items:detail", kwargs={'pk': pk}))

@login_required
def delete_review(request, review_pk): #리뷰 삭제에 대한 메소드
    try:
        review = models.Review.objects.get(pk=review_pk)
        item_pk = review.item.pk
        user = request.user
        if user == review.user or user.is_superuser:
            review.delete()
            pass
        if request.GET.get("m") == "admin":
            return redirect(reverse("reviews:list"))
        return redirect(reverse("items:detail", kwargs={"pk":item_pk}))
    except models.Review.DoesNotExist:
        pass
    
class EditReviewView(UpdateView): # 리뷰 편집에 관한 클래스
    
    model = models.Review
    template_name = "reviews/review_edit.html"
    pk_url_kwarg = "review_pk"
    success_message = "review Updated"
    fields = (
        "title",
        "content",
        )
    
    def get_object(self): # 편집된 리뷰를 반환하는 메소드
        review_pk = self.kwargs.get("review_pk")
        review = models.Review.objects.get(pk=review_pk)
        return review
    
    def get_success_url(self): # 성공적으로 변환시키고 페이지를 반환하는 메소드
        review_pk = self.kwargs.get("review_pk")
        review = models.Review.objects.get(pk=review_pk)
        return reverse("items:detail", kwargs={"pk":review.item.pk})
    
class ReviewView(ListView): # 리뷰를 나타낼 클래스 정의
    model = models.Review.objects.all()
    template_name = "reviews/review_list.html"
    context_object_name = "reviews"
    
    paginate_by = 10
    paginate_orphans = 5
    ordering = "-created"
    
    def get_queryset(self): # 리뷰 리스트를 반환하는 메소드
        search = self.request.GET.get("search")
        if search is not None:
            review = models.Review.objects.filter(Q(title__icontains=search)|Q(content__icontains=search)|Q(user__name__icontains=search)|Q(item__name__icontains=search)).order_by("-created")
        else:
            review = models.Review.objects.all().order_by("-created")
        if not self.request.user.is_superuser:
            raise Http404()
        return review
    
# def create_order(request, item_pk):
#     user = request.user
#     option_pk = request.POST['option']
      
#     try:
#         item = item_models.Item.objects.get(pk=item_pk)
#         option = item_models.Option.objects.get(pk=option_pk)
#         price = (item.price + option.extra_money)
#         order = models.OrderList.objects.create(
#             item=item,
#             option=option,
#             user=user,
#             price=price,
#         )
#         order.save()
#         return redirect(reverse("core:home"))
#     except models.OrderList.DoesNotExist:
#         return redirect(reverse("core:home"))