from django.contrib import messages
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.shortcuts import redirect, reverse
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin

class LoggedOutOnlyView(UserPassesTestMixin): # 권한 여부 판단 메소드
    def test_func(self):
        return not self.request.user.is_authenticated
    
    def handle_no_permission(self):
        # messages.error(self.request, "Can't go there")
        return redirect("core:home")
    
class LoggedInOnlyView(LoginRequiredMixin): # 로그인 뷰
    
    login_url = reverse_lazy("users:login")