from django import forms
from . import models
from items import models as item_models

class CreateQuestionForm(forms.ModelForm): # 문의 사항을 만들 때 보여줄 폼
    class Meta:
        model = models.Question
        fields=(
            "title",
            "content",
        )
        
    def save(self, item_pk): # 문의사항을 저장하는 메소드
        question = super().save(commit=False)
        item = item_models.Item.objects.get(pk=item_pk)
        question.item = item
        return question
    
class CreateAnswerForm(forms.ModelForm): # 답변을 달때 보여줄 폼에 대한 클래스
    class Meta:
        model = models.Answer
        fields=(
            "title",
            "content",
        )
        
    def save(self, question_pk): # 답변 저장 메소드
        answer = super().save(commit=False)
        question = models.Question.objects.get(pk=question_pk)
        answer.question = question
        question.answer_status = "답변완료"
        question.save()
        return answer