from django.urls import path
from . import views

app_name = "items"

urlpatterns = [ # 상품 관련된 url 리스트
    # path("",views.ItemByOuterView.as_view(), name="itembycategory"),
    path("<int:pk>/", views.ItemDetail.as_view(), name="detail"),
    path("create/", views.CreateItemView.as_view(), name="create"),
    path("<int:pk>/edit/", views.EditItemView.as_view(), name="edit"),
    path("<int:pk>/delete/", views.delete_item, name="delete"),
    path("<int:pk>/photos/", views.ItemPhotosView.as_view(), name="photos"),
    path("<int:pk>/photos/add/", views.AddPhotoView.as_view(), name="add-photo"),
    path("<int:item_pk>/photos/<int:photo_pk>/delete/", views.delete_photo, name="delete-photo"),
    path("<int:item_pk>/photos/<int:photo_pk>/edit/", views.EditPhotoView.as_view(), name="edit-photo"),
    path("admin/",views.AdminView.as_view(), name="admin-page"),
    path("categories/", views.CategoryView.as_view(), name="categories"),
    path("categories/add/", views.AddCategoryView.as_view(), name="add-category"),
    path("categories/<int:category_pk>/delete/", views.delete_category, name="delete-category"),
    path("categories/<int:category_pk>/edit/", views.EditCategoryView.as_view(), name="edit-category"),
    path("tags/", views.TagView.as_view(), name="tags"),
    path("tags/add/", views.AddTagView.as_view(), name="add-tag"),
    path("tags/<int:tag_pk>/delete/", views.delete_tag, name="delete-tag"),
    path("tags/<int:tag_pk>/edit/", views.EditTagView.as_view(), name="edit-tag"),
    path("<int:item_pk>/options/", views.OptionView.as_view(), name="options"),
    path("<int:item_pk>/options/add/", views.AddOptionView.as_view(), name="add-option"),
    path("options/<int:option_pk>/delete/", views.delete_option, name="delete-option"),
    path("options/<int:option_pk>/edit/", views.EditOptionView.as_view(), name="edit-option"),
]