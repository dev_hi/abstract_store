from django.db.models import Q
from django.shortcuts import render, redirect, reverse
from django.views.generic import FormView, UpdateView, ListView
from django.contrib.auth.decorators import login_required
from . import forms, models
from items import models as item_models

# Create your views here.
class CreateQuestion(FormView): # 질문 생성 메소드
    form_class = forms.CreateQuestionForm
    template_name = "qnas/question/question_form.html"
    
    def form_valid(self, form):
        pk = self.kwargs.get('item_pk')
        question = form.save(pk)
        question.user = self.request.user
        question.save()
        return redirect(reverse("items:detail", kwargs={'pk': pk}))
    
@login_required
def delete_question(request, question_pk): # 질문 제거 메소드
    try:
        question = models.Question.objects.get(pk=question_pk)
        item_pk = question.item.pk
        user = request.user
        if user == question.user or user.is_superuser:
            question.delete()
            pass
        return redirect(reverse("items:detail", kwargs={"pk":item_pk}))
    except models.Question.DoesNotExist:
        pass
    
class EditQuestionView(UpdateView): # 질문 편집 메소드
    
    model = models.Question
    template_name = "qnas/question/question_edit.html"
    pk_url_kwarg = "question_pk"
    success_message = "question Updated"
    fields = (
        "title",
        "content",
        )
    
    def get_object(self): # 편집된 질문을 반환할 메소드
        question_pk = self.kwargs.get("question_pk")
        question = models.Question.objects.get(pk=question_pk)
        return question
    
    def get_success_url(self): # 편집후 페이지 전환을 위한 메소드
        question_pk = self.kwargs.get("question_pk")
        question = models.Question.objects.get(pk=question_pk)
        return reverse("items:detail", kwargs={"pk":question.item.pk})
    
class QnAFView(ListView): # 질의응답을 나타낼 폼
    model = models.Question.objects.filter(answer_status="미답변")
    template_name = "qnas/qna_F_list.html"
    context_object_name = "questions"
    
    paginate_by = 10
    paginate_orphans = 5
    ordering = "-created"
    
    def get_queryset(self): # 질의응답 리스트를 반환할 메소드
        search = self.request.GET.get("search")
        if search is not None:
            question = models.Question.objects.filter((Q(title__icontains=search)|Q(content__icontains=search)|Q(user__name__icontains=search)|Q(item__name__icontains=search))&Q(answer_status="미답변")).order_by("-created")
        else:
            question = models.Question.objects.filter(answer_status="미답변").order_by("-created")
        if not self.request.user.is_superuser:
            raise Http404()
        return question
    
class QnATView(ListView): # 답변이 완료된 질의응답 뷰를 보여줄 클래스
    model = models.Question.objects.filter(answer_status="답변완료")
    template_name = "qnas/qna_T_list.html"
    context_object_name = "questions"
    
    paginate_by = 8
    paginate_orphans = 4
    ordering = "-created"
    
    def get_queryset(self): # 리스트 형태로 보여줄 메소드
        search = self.request.GET.get("search")
        if search is not None:
            question = models.Question.objects.filter((Q(title__icontains=search)|Q(content__icontains=search)|Q(user__name__icontains=search)|Q(item__name__icontains=search))&Q(answer_status="답변완료")).order_by("-created")
        else:
            question = models.Question.objects.filter(answer_status="답변완료").order_by("-created")
        if not self.request.user.is_superuser:
            raise Http404()
        return question
    
class CreateAnswer(FormView): # 답변 생성 클래스
    form_class = forms.CreateAnswerForm
    template_name = "qnas/answer/answer_form.html"
    
    def form_valid(self, form): # 답변이 유효한지 확인할 메소드
        pk = self.kwargs.get('question_pk')
        answer = form.save(pk)
        answer.user = self.request.user
        answer.save()
        if self.request.GET.get("m") == "admin":
            return redirect(reverse("qnas:list-f"))
        return redirect(reverse("items:detail", kwargs={'pk': answer.question.item.pk}))
    
@login_required
def delete_answer(request, answer_pk): # 답변 삭제 메소드
    try:
        answer = models.Answer.objects.get(pk=answer_pk)
        question = answer.question  
        item_pk = question.item.pk
        if request.user.is_superuser:
            print("Tsetsetset :",question.answers.all().count())
            if question.answers.all().count() < 2:
                question.answer_status = "미답변"
                question.save()
    
            answer.delete()
            pass
        if request.GET.get("m") == "admin":
            return redirect(reverse("qnas:list-t"))
        return redirect(reverse("items:detail", kwargs={"pk":item_pk}))
    except models.Answer.DoesNotExist:
        pass
    
class EditAnswerView(UpdateView): # 답변 편집 클래스
    
    model = models.Answer
    template_name = "qnas/answer/answer_edit.html"
    pk_url_kwarg = "answer_pk"
    success_message = "answer Updated"
    fields = (
        "title",
        "content",
        )
    
    def get_object(self): # 편집된 답변을 보여줄 메소드
        answer_pk = self.kwargs.get("answer_pk")
        answer = models.Answer.objects.get(pk=answer_pk)
        return answer
    
    def get_success_url(self): # 성공적으로 편집한후 페이지 이동을 위한 메소드
        answer_pk = self.kwargs.get("answer_pk")
        answer = models.Answer.objects.get(pk=answer_pk)
        if self.request.GET.get("m") == "admin":
            return reverse("qnas:list-t")
        return reverse("items:detail", kwargs={"pk":answer.question.item.pk})