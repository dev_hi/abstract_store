from django.urls import path
from . import views

app_name = "reviews"

urlpatterns = [ # 리뷰와 관련된 url 리스트
    path("", views.ReviewView.as_view(), name="list"),
    path("form/<int:item_pk>/", views.CreateReview.as_view(), name="review-form"),
    path("<int:review_pk>/delete/", views.delete_review, name="delete"),
    path("<int:review_pk>/edit/", views.EditReviewView.as_view(), name="edit"),
]
