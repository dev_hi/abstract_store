from django.db import models

class CustomModelManager(models.Manager): # 모델 관리자에 대한 정의
    
    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist:
            return None