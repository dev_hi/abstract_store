from django import forms
from . import models
from items import models as item_models

class CreateReviewForm(forms.ModelForm): # 리뷰 생성에 관한 폼을 정의한 메소드
    class Meta:
        model = models.Review
        fields=(
            "title",
            "content",
        )
        
    def save(self, item_pk): # 리뷰 저장 메소드
        review = super().save(commit=False)
        item = item_models.Item.objects.get(pk=item_pk)
        review.item = item
        return review