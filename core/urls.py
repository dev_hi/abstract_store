from django.urls import path
from items import views as item_views

app_name = "core"

urlpatterns = [ # 메인페이지 url 정의
    path("", item_views.HomeView.as_view(), name="home"),
]
