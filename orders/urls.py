from django.urls import path
from . import views

app_name = "orders"

urlpatterns = [ # 주문과 관련된 url 정의 리스트
    path('', views.OrderList.as_view(), name="all"),
    path('complete/', views.order_pay_complete_notify.as_view(), name="complete"),
    path('cancel/', views.order_cancel_notify.as_view(), name="cancel"),
    path('error/', views.order_error_notify.as_view(), name="error"),
    path("<int:pk>/", views.OrderDetail.as_view(), name="detail"),
    path("<int:order_id>/callback/", views.order_callback, name="callback"),
    path("<int:order_pk>/delete/", views.delete_order, name="delete-order"),
    path("<int:order_pk>/pay/cancel/", views.delete_order, name="cancel-pay"),
]