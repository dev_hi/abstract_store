class StoreAPI {
    constructor(user_id) {
        this.MessageReceiverRegistered = false;
        this.isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
        this.API_ROOT = '/api';
        this.UserID = user_id;
        this.Body = document.getElementsByTagName('body')[0];
    }
    async GET(url) {
        return await StoreAPI.Request('GET', this.API_ROOT + url);
    }
    async POST(url, body) {
        return await StoreAPI.Request('POST', this.API_ROOT + url, body);
    }
    async PUT(url, body) {
        return await StoreAPI.Request('PUT', this.API_ROOT + url, body);
    }
    async PATCH(url, body) {
        return await StoreAPI.Request('PATCH', this.API_ROOT + url, body);
    }
    async DELETE(url) {
        return await StoreAPI.Request('DELETE', this.API_ROOT + url); 
    }
    static csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    static async Request(method, url, body) {
        if(fetch != undefined) {
            let option = {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept' : 'application/json'
                }
            };
            if(!StoreAPI.csrfSafeMethod(method)) {
                option.headers['X-CSRFToken'] = StoreAPI.getCookie('csrftoken');
            }
            if(body != undefined && body != null) {
                option.body = JSON.stringify(body);
            }
            return (await fetch(url, option)).json();
        }
        else {
            let xhr = new XMLHttpRequest();
            xhr.open(method, url, true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.setRequestHeader('Accept', 'application/json');
            if(!StoreAPI.csrfSafeMethod(method)) {
                xhr.setRequestHeader('X-CSRFToken', StoreAPI.getCookie('csrftoken'));
            }
            xhr.onreadystatechange = function(a) {
                if(a.currentTarget.readyState == 4) {
                    if(a.currentTarget.status == 200) {
                        resolve(JSON.parse(a.currentTarget.response));
                    }
                    else {
                        reject("request failed, http status = " + status);
                    }
                }
            }
            if(body != undefined && body != null) {
                xhr.send(JSON.stringify(body));
            }
            else {
                xhr.send();
            }
        }
    }
    async GetSession() {
        return await this.GET('/user/session');
    }
    async AddFav(item_id) {
        let user = await this.GetSession();
        let favlist = user.favlist;
        if(favlist.indexOf(item_id) == -1) {
            favlist.push(item_id);
            return await this.PUT('/user/session', {
                favlist: favlist
            });
        }
        else {
            return user;
        }
    }
    async RemoveFav(item_id) {
        let user = await this.GetSession();
        let favlist = user.favlist;
        let idx = favlist.indexOf(item_id);
        if(idx > -1) {
            favlist.splice(idx, 1);
            return await this.PUT('/user/session', {
                favlist: favlist
            });
        }
        else {
            return user;
        }
    }
    async CreateOrder(items) {
        let order = await this.POST("/order/create", {
            user_id: this.UserID,
            items: items
        });
        await this.SetUserCurrentOrder(order.order_id);
        return order;
    }
    async GetOrders() {
        return await this.GET("/user/" + this.UserID + "/orders");
    }
    async GetUserCurrentOrder() {
        return await this.GET("/user/" + this.UserID + "/current_order");
    }
    async SetUserCurrentOrder(order_id) {
        return await this.PUT('/user/' + this.UserID + "/current_order", {'order_id' : order_id});
    }
    async GetOrder(id) {
        return await this.GET("/order/" + id);
    }
    async SetOrderState(order_id, state) {
        return await this.PUT("/order/" + order_id, {state:state});
    }
    async SetOrderDeliveryID(order_id, did) {
        return await this.PUT("/order/" + order_id, {delivery_id:did});
    }
    async OrderItems(items) {
        let resp = await this.GetUserCurrentOrder();
        let order = resp.order_id;
        console.log(resp, order);
        if(order == null) {
            for(let i = 0; i < items.length; ++i) {
                if(items[i].option == undefined || items[i].option < 0) items[i].option = [];
            }
            return await this.CreateOrder(items);
        }
        else {
            let ret = {};
            for(let i = 0; i < items.length; ++i) {
                ret = await this.AddItemToOrder(order, items[i].item, items[i].option, items[i].quantity);
            }
            return ret;
        }
    }
    async AddItemToCurrentOrderOrCreate(item_id, option_index, quantity) {
        let resp = await this.GetUserCurrentOrder();
        let order = resp.order_id;
        console.log(resp, order);
        if(order == null) {
            let oi = {
                item: item_id,
                option: [],
                quantity: quantity
            };
            if(option_index != undefined && option_index >= 0) oi.option.push(option_index);
            return await this.CreateOrder([oi]);
        }
        else {
            return await this.AddItemToOrder(order, item_id, option_index, quantity);
        }
    }
    async AddItemToOrder(order_id, item_id, option_index, quantity) {
        let order = await this.GetOrder(order_id);
        
        let body = {
            items: order.items
        };
        var updated = false;
        for(let i = 0; i < body.items.length; ++i) {
            if(body.items[i].item == item_id) {
                if(option_index != undefined && option_index >= 0) {
                    if(body.items[i].option[0] == option_index) {
                        body.items[i].quantity += quantity;
                        updated = true;
                    }
                }
                else {
                    if(body.items[i].option.length == 0) {
                        body.items[i].quantity += quantity;
                        updated = true;
                    }
                }
                if(updated) break;
            }
            if(updated) break;
        }
        console.log(updated, body, arguments);
        if(!updated) {
            let oi = {
                item: item_id,
                option: [],
                quantity: quantity
            };
            if(option_index != undefined && option_index >= 0) oi.option.push(option_index);
            body.items.push(oi);
        }
        if(option_index != undefined && option_index >= 0) body.items[0].option.push(option_index);
        
        return await this.PUT("/order/" + order_id, body);
    }
    async RemoveItemFromOrder(order_id, item_index) {
        let body = {
            items_remove: item_index
        }
        return await this.PUT("/order/" + order_id, body);
    }
    async RemoveItemById(order_id, item_id, option) {
        let order = await this.GetOrder(order_id);
        let i = 0;
        let found = false;
        for(; i < order.items.length; ++i) {
            if(order.items[i].item == item_id && order.items[i].option[0] == option) {
                found = true;
                break;
            }
            if(found) break;
        }
        if(found) {
            return await this.RemoveItemFromOrder(order_id, i);
        }
        return order;
    }
    async SetOIQuantity(oid, quantity) {
        return await this.PUT('/oi/' + oid, {quantity: parseInt(quantity)});
    }
    async RemoveOrder(order_id) {
        return await this.DELETE('/order/' + order_id);
    }
    async GetPaymentURL(order_id) {
        return await this.GET("/order/" + order_id +  "/payment/url");
    }
    async CancelPayment(order_id) {
        return await this.DELETE('/order/' + order_id + '/payment');
    }
    async VerifyPayment(order_id) {
        return await this.GET('/order/' + order_id + '/payment');
    }
    async GetItemCount(item_id) {
        return await this.GET('/item/' + item_id + '/count');
    }
    async SetItemCount(item_id, count) {
        return await this.PUT('/item/' + item_id + '/count', {count:count});
    }
    async AddItemCount(item_id, amount) {
        let current = (await this.GetItemCount(item_id)).count;
        console.log(current);
        return await this.SetItemCount(item_id, current + parseInt(amount));
    }
    async DeleteOrderingItem(oid) {
        return await this.DELETE('/oi/' + oid);
    }
    static GetOrderID() {
        let spl = document.URL.split('/');
        let id = 0;
        for(let i = 0 ; i < spl.length; ++i) {
            if(spl[i] == "orders" && i + 1 < spl.length) id = parseInt(spl[i + 1]);
        }
        return id;
    }
    static GetItemID() {
        let spl = document.URL.split('/');
        let id = 0;
        for(let i = 0 ; i < spl.length; ++i) {
            if(spl[i] == "items" && i + 1 < spl.length) id = parseInt(spl[i + 1]);
        }
        return id;
    }
    OpenPayment(order_id) {
        let self = this;
        this.GetPaymentURL(order_id).then(function (res) {
            self.OpenIframe(res.url);
        });
    }
    MessageReceiver() {
        var self = this;
        return function(e) {
            self.CloseIframe();
            console.log(e);
            if(e.data == 'completed') {
                window.location.reload(true);
            }
            else if(e.data == 'cancelled') {
                alert('결제가 취소되었습니다.');
            }
            else {
                alert('결제가 취소되었습니다.');
            }
        }
    }
    OpenIframe(url) {
        var self = this;
        if(!window.MessageReceiverRegistered){
            window.addEventListener('message', this.MessageReceiver());
            window.MessageReceiverRegistered = true;
        }
        if(this.isChrome) {
            var popup = window.open(url, "payment_window", "location=no,menubar=no,status=no,titlebar=no,toolbar=no");
        }
        else {
            this.CurrentIframe = document.createElement('iframe');
            this.CurrentIframe.classList.add('payment-frame');
            this.CurrentIframe.setAttribute('scrolling', 'no');
            this.CurrentIframe.setAttribute('frameborder', '0');
            this.CurrentIframe.setAttribute('allowtransparency', 'true');
            this.CurrentIframe.setAttribute('sandbox', 'allow-same-origin allow-scripts allow-forms allow-popups');
            this.Body.appendChild(this.CurrentIframe);
            this.CurrentIframe.setAttribute('src', url);
        }
    }
    CloseIframe() {
        if(this.CurrentIframe != undefined) {
            this.Body.removeChild(this.CurrentIframe);
            this.CurrentIframe = undefined;
        }
    }
    static getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = cookies[i].trim();
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    static async GetObject() {
        let user = await StoreAPI.Request('GET', '/api/user/session');
        if(user.error == undefined) return new StoreAPI(user.id);
        else reject('unauthorized');
    }
}