# Generated by Django 2.2.5 on 2020-06-29 16:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0008_remove_user_admin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='current_order',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='users', to='orders.Order'),
        ),
    ]
